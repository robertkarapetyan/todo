import './App.css';
import React from 'react';
import ListItem from './ListItem.js'
import { nanoid } from 'nanoid'

class App extends React.Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
    this.removeLi = this.removeLi.bind(this);
    this.check = this.check.bind(this);
    this.state = { taskList: [], inputVal: "", filter: "all", };
  };
  // task object {value, checked} 

  handleSubmit(event) {
    event.preventDefault();
    let input = this.state.inputVal;
    if (input.length === 0 || this.state.taskList.map((e) => e[0]).includes(input)) {
      alert("Invalid input:");
      return;
    }

    const newTask = {
      value: input,
      checked: false,
    }

    this.setState(state => {
      return {
        taskList: this.state.taskList.concat(newTask),
        inputVal: "",
        // filter: "all",
      }
    })
    return false;
  }

  handleInputChange(event) {
    this.setState({ inputVal: event.target.value });
  }

  handleFilter(event) {
    this.setState(state => {
      return {
        filter: event.target.value.toLowerCase()
      }
    })
  }

  removeLi(liName) {
    this.setState({
      taskList: this.state.taskList.filter((task) => {
        return task.value !== liName;
      })
    }
    )
  }

  check(liName) {
    this.setState({
      taskList: this.state.taskList.map((task) => {
        if (task.value === liName) {
          return { value: liName, checked: !task.checked };
        }
        return task;
      })
    })
  }

  render() {
    // li key uniqueness fixed
    return (
      <div style={{ justifyContent: 'center', display: 'grid', zoom: '180%', marginTop: '60px' }}>
        <form style={{ display: 'flex', justifyContent: 'center' }} onSubmit={this.handleSubmit} >
          <input
            onChange={this.handleInputChange}
            placeholder="New task"
            value={this.state.inputVal} />
          <button>
            Add to list
          </button>
          <div onChange={this.handleFilter}>
            <label style={{ marginLeft: '20px' }}>Filter by:</label>

            <input type="radio" value="All" id="showAllT" name="doneFilter"></input>
            <label htmlFor="showAllT">All</label>
            <input type="radio" value="Done" id="showDoneT" name="doneFilter"></input>
            <label htmlFor="showDoneT">Done</label>
            <input type="radio" value="Left" id="showLeftT" name="doneFilter"></input>
            <label htmlFor="showLeftT">Left</label>
          </div>
        </form>
        <ol>
          {this.state.taskList.map(li => (
            <ListItem
              key={nanoid()} // key={li.value} also works since each value is unique
              item={li}
              filter={this.state.filter}
              remove={this.removeLi}
              check={this.check}
            />
          ))}
        </ol>
      </div>
    )
  }

}

export default App;
