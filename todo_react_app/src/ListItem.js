import React from 'react';

class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);
    }

    handleCheck() {
        this.props.check(this.props.item.value);
    }
    render() {
        let listItem;
        switch (this.props.filter) {
            case "all": {
                if (this.props.item.checked === true) {
                    listItem =
                        <li>
                            <label style={{ textDecorationLine: 'line-through' }}>{this.props.item.value}</label>
                            <input type="checkbox" defaultChecked="true" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>;
                }
                else {
                    listItem =
                        <li>
                            <label>{this.props.item.value}</label>
                            <input type="checkbox" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>;
                }
                break;
            }
            case "done": {
                if (this.props.item.checked === true) {
                    listItem =
                        <li>
                            <label style={{ textDecorationLine: 'line-through' }}>{this.props.item.value}</label>
                            <input type="checkbox" defaultChecked="true" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>
                        ;
                }
                else listItem = null;
                break;
            }
            case "left": {
                if (this.props.item.checked === false) {
                    listItem =
                        <li>
                            <label>{this.props.item.value}</label>
                            <input type="checkbox" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>
                        ;
                }
                else listItem = null;
                break;
            }
            default:
                listItem = null;;
                break;
        }
        return listItem;
    }
}

export default ListItem;