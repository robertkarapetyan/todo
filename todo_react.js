'use strict';
class TodoApp extends React.Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleFilter = this.handleFilter.bind(this);
        this.removeLi = this.removeLi.bind(this);
        this.check = this.check.bind(this);
        this.state = { taskList: [], inputVal: "", filter: "all", };
    };
    // task object {value, checked} 

    handleSubmit(event) {
        event.preventDefault();
        let input = this.state.inputVal;
        if (input.length === 0 || this.state.taskList.map((e) => e.value).includes(input)) {
            alert("Invalid input:");
            return;
        }

        const newTask = {
            value: input,
            checked: false,
        }

        this.setState(state => {
            return {
                taskList: this.state.taskList.concat(newTask),
                inputVal: "",
                // filter: "all",
            }
        })
        return false;
    }

    handleInputChange(event) {
        this.setState({ inputVal: event.target.value });
    }

    handleFilter(event) {
        this.setState(state => {
            return {
                filter: event.target.value.toLowerCase()
            }
        })
    }

    removeLi(liName) {
        this.setState({
            taskList: this.state.taskList.filter((task) => {
                return task.value != liName;
            })
        }
        )
    }

    check(liName) {
        this.setState({
            taskList: this.state.taskList.map((task) => {
                if (task.value === liName) {
                    return { value: liName, checked: !task.checked };
                }
                return task;
            })
        })
    }

    render() {
        return (
            <div style={{ justifyContent: 'center', display: 'grid', zoom: '180%' }}>
                <form style={{ display: 'flex', justifyContent: 'center' }} onSubmit={this.handleSubmit} >
                    <input
                        onChange={this.handleInputChange}
                        placeholder="New task"
                        value={this.state.inputVal} />
                    <button>
                        Add to list
                    </button>
                    <div onChange={this.handleFilter}>
                        <label style={{ marginLeft: '20px' }}>Filter by:</label>

                        <input type="radio" value="All" id="showAllT" name="doneFilter"></input>
                        <label htmlFor="showAllT">All</label>
                        <input type="radio" value="Done" id="showDoneT" name="doneFilter"></input>
                        <label htmlFor="showDoneT">Done</label>
                        <input type="radio" value="Left" id="showLeftT" name="doneFilter"></input>
                        <label htmlFor="showLeftT">Left</label>
                    </div>
                </form>
                <ol>
                    {this.state.taskList.map(li => (
                        <ListItem
                            key={li.value} // each value is unique
                            item={li}
                            filter={this.state.filter}
                            remove={this.removeLi}
                            check={this.check} 
                        />
                    ))}
                </ol>
            </div>
        )
    }
}
class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.handleCheck = this.handleCheck.bind(this);
    }

    handleCheck() {
        this.props.check(this.props.item.value);
    }

    // li key uniqueness fixed
    render() {
        let listItem;
        switch (this.props.filter) {
            case "all": {
                if (this.props.item.checked === true) {

                    listItem =
                        <li>
                            <label style={{ textDecorationLine: 'line-through' }}>{this.props.item.value}</label>
                            <input type="checkbox" defaultChecked="true" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>;
                }
                else {
                    listItem =
                        <li>
                            <label>{this.props.item.value}</label>
                            <input type="checkbox" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>;
                }
                break;
            }
            case "done": {
                if (this.props.item.checked === true) {
                    listItem =
                        <li>
                            <label style={{ textDecorationLine: 'line-through' }}>{this.props.item.value}</label>
                            <input type="checkbox" defaultChecked="true" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>
                        ;
                }
                else listItem = null;
                break;
            }
            case "left": {
                if (this.props.item.checked === false) {
                    listItem =
                        <li>
                            <label>{this.props.item.value}</label>
                            <input type="checkbox" onChange={this.handleCheck}></input>
                            <button onClick={() => this.props.remove(this.props.item.value)}>remove</button>
                        </li>
                        ;
                }
                else listItem = null;
                break;
            }
            default:
                listItem = null;;
                break;
        }
        return listItem;
    }
}

const domContainer = document.querySelector('#react_app');
ReactDOM.render(React.createElement(TodoApp), domContainer);